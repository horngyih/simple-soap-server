/* globals require, module, __dirname */

var _express = require("express");
var _lineReaderSync = require( "line-reader-sync" );

module.exports = function (options) {
    var router = _express.Router();
    var readRQSource = null;
    
    //router.all( '*', parseSoapMessage );
    //router.all( '*', handleMessage );

    router.post('*', parseSoapMessage);
    router.post('*', handleMessage);

    var messageHandlers = {
        'OTA_READRQ': readRequest,
        'OTA_HOTELRATEPLANRQ' : requestRatePlan,
        'OTA_HOTELAVAILGETRQ' : requestAvailability,
        'OTA_NOTIFREPORTRQ' : notifyReport
    };

    function parseSoapMessage(req, res, next) {
        if (req.body) {
            console.log(req.body);
            if (typeof req.soapMessage === "undefined") {
                req.soapMessage = req.body;
            }
        }
        if (next) {
            next();
        }
    }

    function handleMessage(req, res, next) {
        if (req.soapMessage) {
            for (var i in req.soapMessage) {
                var requestMessage = i;
                requestMessage = requestMessage.toUpperCase();
                var handler = messageHandlers[requestMessage];
                if (typeof handler === "function") {
                    handler(req, res, next);
                } else {
                    defaultHandler(req, res, next);
                }
            }
        }
    }

    function readRequest(req, res, next) {
        console.log("Read Request");
        if (res) {
            var responseXML = '<OTA_ResRetrieveRS ';
            responseXML += 'TimeStamp="';
            var now = new Date();
            responseXML += now.toISOString();
            responseXML += '" Version="0" xmlns="http://www.opentravel.org/OTA/2003/05">';
            res.writeHead(200, {
                'Content-Type': 'text/xml; charset=UTF-8'
            });
            
            console.log( __dirname );
            console.log( options );
            if( options.dataSource ){
                console.log( "Preparing data source..." );
                var readRQSourceFile = 'otaReadRq.xml' || options.otaReadRQSource;
                var sourcePath = __dirname + "/" + options.dataSource;
                if( sourcePath.charAt( sourcePath.length -1 ) !== '/' ){
                    sourcePath += '/';
                }
                sourcePath += readRQSourceFile;
                
                if( readRQSource === null ){
                    try{
                        console.log( "Reading from " + sourcePath );
                        readRQSource = new _lineReaderSync( sourcePath );
                    }
                    catch( error ){
                        console.error( error );
                    }
                }
                
            }
            
            if( readRQSource ){
                responseXML += readRQSource.readline();
                responseXML += "<Success/>";
            }
            else{
                responseXML += '<Warnings><Warning ShortText="No Records Found" Code="-5" /></Warnings>';
            }

            responseXML += '</OTA_ResRetrieveRS>';
            console.log( "Response : " );
            console.log( responseXML );
            res.write(responseXML);
        }

        console.log("End response");
        if (res) {
            res.end('');
        }

        if (next) {
            next();
        }
    }

    function requestRatePlan( req, res, next ){
        if( next ){
            next();
        }
    }
    
    function requestAvailability( req, res, next ){
        if( next ){
            next();
        }
    }
  
    function notifyReport( req, res, next ){
        if( next ){
            next();
        }
    }
    
    function defaultHandler(req, res, next) {
        console.log("Default handler");
        console.log(req.body);
        if (next) {
            next();
        }
    }
    return router;
};