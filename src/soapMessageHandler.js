/* globals module */
var SOAP_ENVELOPE = 'soap:envelope';
var SOAP_BODY = 'soap:body';

var soapEnvelopHeader = '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">\n';
var soapEnvelopFooter = '</soap:Envelope>\n';

var soapBodyHeader = '<soap:Body>\n';
var soapBodyFooter = '</soap:Body>\n';


var parseSoapBody = function( req, res, next ){
    if( req.body ){
        if( req.body[SOAP_ENVELOPE] ){
            var envelop = req.body[SOAP_ENVELOPE];
            if( envelop[SOAP_BODY] ){
                var soapBody = envelop[SOAP_BODY];
                req.body = soapBody;
                req.soapMessage = soapBody[0];
                console.log( soapBody );
            }
        }
    }
    
    if( next ){
        next();
    }
};

var wrapSoapBody = function( req, res, next ){
    var writeClosure = function(){
        var responseWrite = res.write;
        var firstWrite = false;
        return function(){
            if( firstWrite === false ){
                responseWrite.call( res, soapEnvelopHeader );
                responseWrite.call( res, soapBodyHeader );
                firstWrite = true;
            }
            responseWrite.apply( res, arguments );
        };
    };
    
    var endClosure = function(){
        var responseEnd = res.end;
        return function(){
            res.write.apply( res, arguments );
            res.write.call( res, soapBodyFooter );
            responseEnd.call( res, soapEnvelopFooter );
        };
    };
    
    res.write = writeClosure();
    res.end = endClosure();
    
    if( next ){
        next();
    }
};

module.exports ={
    parseSoapBody : parseSoapBody,
    wrapSoapBody : wrapSoapBody
};