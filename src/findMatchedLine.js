/* globals require */

var _fs = require('fs');
var _readline = require('readline');

var args = process.argv.slice(2);

var count = 1;
if( args ){
    if( args.length > 1 ){
        try{
            count = parseInt( args[1] );
            if( count < 1 ){
                count = 1;
            }
        }
        catch( error ){
            console.log( "Illegal value for count : " + args[1] );
        }
    }

    if( args.length > 0 ){
        var targetFile = args[0];
        var lineReader = _readline.createInterface({
            input: _fs.createReadStream( targetFile )
        });

        lineReader.on( 'line', processLine );
    }

}

var counter = 0;
function processLine( line ){
    if( line ){
        var targetExpression = /@@crsid=[0-9]{1,}@@.+ResID_Type="29"\sResID_Value="[^"]+"/;
        var matches = targetExpression.exec( line );
        if( matches ){
            if( matches.length > 0 ){
                console.log( counter + " - " + count );
                console.log( line );
                if( counter > count ){
                    //process.exit(0);
                }
                else{
                    counter++;
                }
            }
        }
    }
}
