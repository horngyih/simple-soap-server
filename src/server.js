/* globals require, process */

var _express = require("express");
var _xmlparser = require("express-xml-bodyparser");
var _soapMessageHandler = require("./soapMessageHandler");
var _ariaAndReservationPullService = require( "./ariaAndReservationPullService" );
var _kioskService = require( "./kioskService" );

var args = process.argv.slice(2);

var serverPort = 9898;
if (args.length > 0) {
    try {
        var portArg = parseInt(args[0]);
        if (typeof portArg === "number") {
            serverPort = portArg;
        }
    } catch (error) {}
}

var server = _express();
server.use( _xmlparser() );
server.use( _soapMessageHandler.parseSoapBody );
server.use( _soapMessageHandler.wrapSoapBody );

var ariaAndReservationPullServiceOpts = {
    dataSource : '../data',
    otaReadRQSource : 'otaReadRQ.xml'
};


var kioskServiceOpts = {
    dataSource : '../data',
    otaReadRQSource : 'otaReadRQ.xml'
};

server.use( "/ARIAndReservationPullService", _ariaAndReservationPullService( ariaAndReservationPullServiceOpts ) );

server.use( "/KioskService", _kioskService( kioskServiceOpts ) );

server.listen(serverPort, function () {
    console.log( "Listening on port " + serverPort );
});